// 1 2 3
// 4 5 6
// 7 8 9

function hasSomeoneWon(myBoard) {
   // 0 1 2
   // 3 4 5
   // 6 7 8
    // horizontal checks
    if(myBoard[0] != '_' && myBoard[0] == myBoard[1] &&
       myBoard[1] == myBoard[2]) return true;
    if(myBoard[3] != '_' && myBoard[3] == myBoard[4] &&
       myBoard[4] == myBoard[5]) return true;
    if(myBoard[6] != '_' && myBoard[6] == myBoard[7] &&
       myBoard[7] == myBoard[8]) return true;
    // vertical checks    
    if(myBoard[0] != '_' && myBoard[0] == myBoard[3] &&
       myBoard[3] == myBoard[6]) return true;
    if(myBoard[1] != '_' && myBoard[1] == myBoard[4] &&
       myBoard[4] == myBoard[7]) return true;
    if(myBoard[2] != '_' && myBoard[2] == myBoard[5] &&
       myBoard[5] == myBoard[8]) return true;
    // diagonal checks
    if(myBoard[0] != '_' && myBoard[0] == myBoard[4] &&
       myBoard[4] == myBoard[8]) return true;
    if(myBoard[2] != '_' && myBoard[2] == myBoard[4] &&
       myBoard[4] == myBoard[6]) return true;
    
    return false;
}

function message(msg) {
    document.getElementById('messages').innerHTML = msg;
}

var board = new Array('_', '_', '_',
                      '_', '_', '_',
                      '_', '_', '_');
var currentPlayer = 'X';
var boardString = "";
var someoneWon = false;

var counter = 0;

function RandomAIPlayer(board) {
  var moves = getValidMoves(board);
  if(moves.length == 0) {
    message("AI: no moves available.");
    return -1; 
  }

  return moves[Math.floor(Math.random() * moves.length)];
} 

function getValidMoves(board) {
  var moves = [];
  for(var i = 0; i < board.length; i++) {
    if(board[i] == '_') moves.push(i+1);
  }
  return moves;
}

function step(answer) {
    var numAnswer = parseInt(answer);

    var validMoves = getValidMoves(board); // [1,2,3,4]

    var index = validMoves.indexOf(numAnswer);

    if(index == -1) message("That is an invalid move. These are the valid moves: " + validMoves);
    else {        
        // the number is valid
        board[numAnswer - 1] = currentPlayer;
        
        if(currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
        
        counter ++;
        someoneWon = hasSomeoneWon(board);
    }
    
    renderHtmlBoard();
    
    if(someoneWon) {
      message("Someone won.");
      return;
    }
    else if(counter == 9) {
      message("You ran out of moves.");
      return;
    }

    // AI player move if it's his move
    if(currentPlayer != 'O') return; 

    var aiMove = RandomAIPlayer(board);
    if(aiMove == -1) return;

    board[aiMove - 1] = currentPlayer;
    currentPlayer = 'X';

    counter ++;
    someoneWon = hasSomeoneWon(board);
    renderHtmlBoard();
    
    if(someoneWon) message("Someone won.");
    else if(counter == 9) message("You ran out of moves.");

}

function renderHtmlBoard() {
    var board2 = document.getElementById('board2');
    var cells = board2.getElementsByClassName('cell');
    if(cells.length != 9) alert('Problem, there are too many cells');
    for(var i = 0; i < cells.length; i ++) {
        if(board[i] == '_') {
            cells[i].innerHTML = "";
        } else {
            cells[i].innerHTML = board[i];
        }
    }
}

function onMoveClick() {
    var value = document.getElementById('answer').value;
    step(value);
    document.getElementById('answer').value = '';
    document.getElementById('currentPlayer').innerHTML = "Current player is: " + currentPlayer;
}

document.getElementById('btnMove').addEventListener('click', onMoveClick);

//while(counter < 9 && !someoneWon) {
//    var answer = prompt(boardString + "\nWhich position would you like to play (1-9)?")
    
//}

